package wh.jh.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import wh.jh.mapper.UserMapper;
import org.springframework.data.redis.core.RedisTemplate;
import wh.jh.pojo.User;
import wh.jh.service.UserService;

import java.util.UUID;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserMapper userMapper;

    @Autowired
    private RedisTemplate<String,String> redisTemplate;
    @Override
    public int register(String userName, String passeord) {
        String id = UUID.randomUUID().toString();
        User user= new User(id,userName,passeord);
        int insert = this.userMapper.insert(user);
        if (insert == 1){
            String redisUser = "user_" + userName;
            redisTemplate.opsForHash().put(redisUser,"userName",userName);
            redisTemplate.opsForHash().put(redisUser,"passeord",passeord);
            return insert;
        }
        return 0;
    }
}
