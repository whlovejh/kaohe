package wh.jh.pojo;

public class User {
    private String id;
    private String userName;
    private String user_password;

    public User() {
    }

    public User(String id, String userName, String user_password) {
        this.id = id;
        this.userName = userName;
        this.user_password = user_password;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getuser_password() {
        return user_password;
    }

    public void setuser_password(String user_password) {
        this.user_password = user_password;
    }

    @Override
    public String toString() {
        return "User{" +
                "id='" + id + '\'' +
                ", userName='" + userName + '\'' +
                ", user_password='" + user_password + '\'' +
                '}';
    }
}
