package wh.jh.controller;

import com.fasterxml.jackson.annotation.JsonTypeInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import wh.jh.service.UserService;

@RestController
@RequestMapping("user")
public class UserController {
    @Autowired
    private UserService userService;

    @RequestMapping("register")
    public String register(String userName,String password){
        int register = userService.register(userName, password);
        if (register == 1){
            return "注册成功";
        }
        return "注册失败";
    }

    @RequestMapping("login")
    public void login(){

    }

}
