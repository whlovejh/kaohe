package wh.jh.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Component;
import wh.jh.pojo.User;

@Component
public interface UserMapper extends BaseMapper<User> {
}
