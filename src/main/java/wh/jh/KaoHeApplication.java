package wh.jh;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;


@MapperScan("wh.jh.mapper")
@SpringBootApplication
@ComponentScan("wh")
public class KaoHeApplication {
    public static void main(String[] args) {
        SpringApplication.run(KaoHeApplication.class, args);
    }
}
